﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SimpleTestAuthentication.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public int Age { get; set; }
        public string Gender { get; set; }
        //навигационные свойства
        public int RoleId { get; set; }
        public Role Role { get; set; }
    }
}