﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTTPContextCookies.Models
{
    public class Book
    {
        [Key]
        [HiddenInput(DisplayValue=false)]
        public int Id { get; set; }
        [Required]
        [Display(Name="Название")]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Цена")]
        public decimal Price { get; set; }
        [Display(Name = "Автор")]
        public string Author { get; set; }
    }
}