﻿using HTTPContextCookies.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTTPContextCookies.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            Session["name"] = "Yuliia"; 
            HttpContext.Response.Cookies["pasport"].Value = "MK232323";
            return View();
        }
        public string GetCookies()
        {
            string c = HttpContext.Request.Cookies["pasport"].Value + " " + Session["name"];
            return c.ToString();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public string GetContext()
        {
            string browser = HttpContext.Request.Browser.Browser;
            string user_agent = HttpContext.Request.UserAgent;
            string url = HttpContext.Request.RawUrl;
            string ip = HttpContext.Request.UserHostAddress;
            string referrer = HttpContext.Request.UrlReferrer == null ? "" : HttpContext.Request.UrlReferrer.AbsoluteUri;
            return "<p>Browser: " + browser + "</p><p>User-Agent: " + user_agent + "</p><p>Url запроса: " + url +
                "</p><p>Реферер: " + referrer + "</p><p>IP-адрес: " + ip + "</p>";
        }
        public void voidContext()
        {
            Response.Write("Наш ответ global");
            string browser = HttpContext.Request.Browser.Browser;
            string user_agent = HttpContext.Request.UserAgent;
            string url = HttpContext.Request.RawUrl;
            string ip = HttpContext.Request.UserHostAddress;
            string referrer = HttpContext.Request.UrlReferrer == null ? "" : HttpContext.Request.UrlReferrer.AbsoluteUri;

            HttpContext.Response.Write(
                "<p>Browser: " + browser + "</p><p>User-Agent: " + user_agent + "</p><p>Url запроса: " + url +
                "</p><p>Реферер: " + referrer + "</p><p>IP-адрес: " + ip + "</p>");
        }
        public ActionResult GetList()
        {
            ViewBag.Message = "Partial View";
            return PartialView("_GetList");
        }
        public ActionResult GetModel()
        {
            IEnumerable<Book> lst = new List<Book>() { new Book() { Name = "Игры разума", Author = "Shildt", Price = 200},
            new Book() { Name = "Игры разума", Author = "Shildt", Price = 200}};
            return PartialView("_GetModel",lst);
        }
        [HttpGet]
        public ActionResult Array()
        {

            return View();
        }
        [HttpPost]
        public string Array(List<string> names)
        {
            string fin = "";
            for (int i = 0; i < names.Count; i++)
            {
                fin += names[i] + ";  ";
            }
            return fin;
        }
        [HttpGet]
        public ActionResult GetAuthor()
        {
            return View();
        }
        [HttpPost]
        public ActionResult GetAuthor(Author author)
        {

            return View(); //поставим точку останова и просмотрим
        }
    }
}