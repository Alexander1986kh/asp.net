﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FishingShop.Models
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public int Discount { get; set; }
        public decimal SelfCost { get; set; }//Цена закупки
        [Required]
        public virtual ICollection<Category> Category { set; get; }
        public virtual ICollection<Basket> Baskets { get; set; }
        public virtual ICollection<Buyer> Buyers { get; set; }
    }
}