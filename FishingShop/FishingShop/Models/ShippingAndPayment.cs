﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FishingShop.Models
{
    public class ShippingAndPayment
    {
        public bool Prepay { get; set; }
        public bool COD { get; set; }
        public DateTime DateOfPayment { get; set; }
        public virtual ICollection<Product> Products { get; set; }       
        public virtual ICollection<Buyer> Buyer { get; set; }
        public virtual ICollection<Manager> Manager { get; set; }
    }
}