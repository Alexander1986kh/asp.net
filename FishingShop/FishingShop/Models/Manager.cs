﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FishingShop.Models
{
    public class Manager
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public decimal Salary { get; set; }
        public virtual ICollection<ShippingAndPayment> ShippingAndPayments { get; set; }
    }
}