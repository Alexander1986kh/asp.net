﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FishingShop.Models
{
    public class Basket
    {
        public DateTime DateOfOrder { get; set; }
        public virtual ICollection<Product> Products { get; set; }
        public virtual Buyer Buyer { get; set; }
    }
}