﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace FishingShop.Models
{
    public class FishingShopContext:DbContext
    {
        public FishingShopContext() : base("FishingShopContext") { }
        public DbSet<Basket> Basket{ get; set; }
        public DbSet<Buyer> Buyer { get; set; }
        public DbSet<Category> Category { get; set; }
        public DbSet<Manager> Manager { get; set; }
        public DbSet<Product> Product { get; set; }
        public DbSet<ShippingAndPayment> ShippingAndPayment { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Basket>()
                .HasMany(b => b.Products)
                .WithMany(p => p.Baskets)
                .Map(t => t.MapLeftKey("Id")//Basket
                .MapRightKey("Id")//Product
                .ToTable("BasketProduct"));

            modelBuilder.Entity<Buyer>()
                .HasMany(b => b.Baskets)
                .WithRequired(b => b.Buyer);

            modelBuilder.Entity<Buyer>()
                .HasMany(b => b.AllPurchases)
                .WithMany(p => p.Buyers)
                .Map(t => t.MapLeftKey("Id")//Buyer
                .MapRightKey("Id")//Product
                .ToTable("BuyerProduct"));

            modelBuilder.Entity<Category>()
                .HasMany(c => c.Products)
                .WithRequired(p => p.Category);

            modelBuilder.Entity<Manager>()
               .HasMany(m => m.ShippingAndPayments)
               .WithRequired(s => s.Manager);

        }
    }
}