﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AjaxSecurityMVC.Startup))]
namespace AjaxSecurityMVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
