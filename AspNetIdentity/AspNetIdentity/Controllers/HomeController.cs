﻿using AspNetIdentity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AspNetIdentity.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        [Authorize(Roles = "admin")]
        public ActionResult GetBooks()
        {
            ApplicationDbContext db = new ApplicationDbContext();
            Item i1 = new Item { Name = "Asp.Net mvc5", Price = 850.0 };
            Item i2 = new Item { Name = "Asp.Net core 2.0", Price = 1250.0 };
            db.Items.Add(i1);
            db.Items.Add(i2);
            db.SaveChanges();
            return View(db.Items.ToList());
        }
        public ActionResult GetUsers()
        {
            List<ApplicationUser> users = new List<ApplicationUser>();
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                users = db.Users.ToList();
            }
            return View(users);
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}