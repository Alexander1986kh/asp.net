﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Web;

namespace PaginationAspMVC.Models
{
    public class MobileContext:DbContext
    {
        public DbSet<Phone> Phones { get; set; }
    }
}