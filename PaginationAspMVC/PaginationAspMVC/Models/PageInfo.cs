﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PaginationAspMVC.Models
{
    public class PageInfo
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public int PageNumber { get; set; } // номер текущей страницы
        [Required]
        public int PageSize { get; set; } // кол-во объектов на странице
        [Required]
        public int TotalItems { get; set; } // всего объектов
        [Required]
        public int TotalPages  // всего страниц
        {
            get { return (int)Math.Ceiling((decimal)TotalItems / PageSize); }
        }

    }
}