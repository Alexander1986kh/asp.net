﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BooksFiltringViewModel.Models
{
    public class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<Book> Books { get; set; }
        public Category()
        {
            Books = new List<Book>();
        }
    }
}