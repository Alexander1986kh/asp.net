﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BooksFiltringViewModel.Models
{
    public class BooksListViewModel
    {
        public IEnumerable<Book> Books { get; set; }//отфильтрованные
        public SelectList Categories { get; set; }
       
    }
}