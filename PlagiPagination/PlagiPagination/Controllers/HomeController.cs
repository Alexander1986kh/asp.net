﻿
using PagedList;
using System.Collections.Generic;
using System.Web.Mvc;
using PlagiPagination.Models;
using System;
using System.Linq;

namespace WebPagination_2.Controllers
{
    public class HomeController : Controller
    {
        #region Plagin Pagination

        //List<Phone> phones;
        //public HomeController()
        //{
        //    phones = new List<Phone>();
        //    phones.Add(new Phone { Id = 1, Name = "Samsung Galaxi" });
        //    phones.Add(new Phone { Id = 2, Name = "Samsung Galaxi II" });
        //    phones.Add(new Phone { Id = 3, Name = "Samsung Galaxi II" });
        //    phones.Add(new Phone { Id = 4, Name = "Samsung ACE" });
        //    phones.Add(new Phone { Id = 5, Name = "Samsung ACE II" });
        //    phones.Add(new Phone { Id = 6, Name = "HTC One S" });
        //    phones.Add(new Phone { Id = 7, Name = "HTC One X" });
        //    phones.Add(new Phone { Id = 8, Name = "Nokia N9" });
        //}
        //public ActionResult Index(int? page)
        //{
        //    int pageSize = 3;//здесь устанавливаем кол-во элементов на страницу
        //    int pageNumber = (page ?? 1);
        //    return View(phones.ToPagedList(pageNumber, pageSize));
        //}
        #endregion
        SoccerContext db = new SoccerContext();

        public ActionResult Index(int? team, string position)
        {
            IQueryable<Player> players = db.Players.Include("Team");
            //для фильтрации
            if (team != null && team != 0)
            {
                players = players.Where(p => p.TeamId == team);
            }
            if (!String.IsNullOrEmpty(position) && !position.Equals("Все"))
            {
                players = players.Where(p => p.Position == position);
            }

            List<Team> teams = db.Teams.ToList();
            // устанавливаем начальный элемент, который позволит выбрать всех
            teams.Insert(0, new Team { Name = "Все", Id = 0 });

            PlayersListViewModel plvm = new PlayersListViewModel
            {
                Players = players.ToList(),
                Teams = new SelectList(teams, "Id", "Name"),
                Positions = new SelectList(new List<string>()
            {
                "Все",//специально добавили
                "Нападающий",
                "Полузащитник",
                "Защитник",
                "Вратарь"
            })
            };
            return View(plvm);
        }
    }
}