﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PlagiPagination.Models
{
    public class PlayersListViewModel
    {
        public int Id { get; set; }
        public IEnumerable<Player> Players { get; set; }//отфильтрованные
        public SelectList Teams { get; set; }
        public SelectList Positions { get; set; }
    }
}