﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PlagiPagination.Models
{
    class SoccerbInitiolizer : DropCreateDatabaseAlways<SoccerContext>
    {
        protected override void Seed(SoccerContext context)
        {
            Team t1 = new Team() { Name = "Металист" };
            Team t2 = new Team() { Name = "Динамо" };
            Team t3 = new Team() { Name = "Шахтер" };
            context.Teams.Add(t1);
            context.Teams.Add(t2);
            context.Teams.Add(t3);
            Player p1 = new Player() { Name = "Name 1", Age = 21, Position = "Нападающий", Team = t1 };
            Player p2 = new Player() { Name = "Name 2", Age = 22, Position = "Полузащитник", Team = t1 };
            Player p3 = new Player() { Name = "Name 3", Age = 23, Position = "Полузащитник", Team = t1 };

            Player p4 = new Player() { Name = "Name 14", Age = 21, Position = "Нападающий", Team = t2 };
            Player p5 = new Player() { Name = "Name 25", Age = 22, Position = "Полузащитник", Team = t2 };
            Player p6 = new Player() { Name = "Name 36", Age = 23, Position = "Полузащитник", Team = t2 };

            Player p7 = new Player() { Name = "Name 11", Age = 21, Position = "Нападающий", Team = t3 };
            Player p8 = new Player() { Name = "Name 21", Age = 22, Position = "Полузащитник", Team = t3 };
            Player p9 = new Player() { Name = "Name 31", Age = 23, Position = "Полузащитник", Team = t3 };
            context.Players.AddRange(new List<Player> { p1, p2, p3, p4, p5, p6, p7, p8, p9 });
            context.SaveChanges();
        }
    }

}