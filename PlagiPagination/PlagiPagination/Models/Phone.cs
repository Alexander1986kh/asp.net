﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PlagiPagination.Models
{
    public class Phone
    {
       // [HiddenInput(SystemValue=null)]
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
    }
}