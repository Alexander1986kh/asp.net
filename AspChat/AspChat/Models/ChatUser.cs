﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AspChat.Models
{
    public class ChatUser
    {
        public string Name;
        public DateTime LoginTime;
        public DateTime LastPing;
    }
}