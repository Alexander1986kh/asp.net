﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ModelBindingAsp.Models
{
    [ModelBinder(typeof(ItemModelBinder))]
    public class Item
    {
        [ScaffoldColumn(false)]
        public int Id { get; set; }

        [Display(Name = "Название книги")]
        public string Name { get; set; }
        [Display(Name = "Цена")]
        [Range(typeof(decimal), "1,0", "1000,6", ErrorMessage = "Наименьшая цена -1$, в качестве разделителя дробной и целой части используется запятая")]
        public decimal Price { get; set; }
        [Display(Name = "Курс долара")]
        [Range(typeof(decimal), "25,0", "30,6", ErrorMessage = "Курс от 25,0$, в качестве разделителя дробной и целой части используется запятая")]
        public decimal KDollar { get; set; }
    }
}