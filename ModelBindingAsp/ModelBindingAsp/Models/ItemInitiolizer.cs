﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ModelBindingAsp.Models
{
    public class ItemInitiolizer : DropCreateDatabaseAlways<ItemContext>
    {
        protected override void Seed(ItemContext context)
        {
            Item p1 = new Item() { Name = "C++ 11", Price = (decimal)220, KDollar = (decimal)26.2 };
            Item p2 = new Item() { Name = "STL", Price = (decimal)420, KDollar = (decimal)26.2 };
            Item p3 = new Item() { Name = "WF", Price = (decimal)220, KDollar = (decimal)26.2 };
            Item p4 = new Item() { Name = "WPF", Price = (decimal)520, KDollar = (decimal)26.2 };

            context.Items.AddRange(new List<Item> { p1, p2, p3, p4 });
            context.SaveChanges();

            base.Seed(context);
        }
    }
}