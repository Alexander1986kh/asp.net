﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Mvc;

namespace ModelBindingAsp.Models
{
    public class ItemModelBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            // Получаем поставщик значений
            var valueProvider = bindingContext.ValueProvider;

            // получаем данные по одному полю
            ValueProviderResult vprId = valueProvider.GetValue("Id");

            // получаем данные по остальным полям
            string name = (string)valueProvider.GetValue("Name").ConvertTo(typeof(string));

            decimal price = (decimal)valueProvider.GetValue("Price").ConvertTo(typeof(decimal));
            decimal kDollar = (decimal)valueProvider.GetValue("KDollar").ConvertTo(typeof(decimal));

            Item item = new Item() { Name = name + " (New)", Price = price * kDollar, KDollar = kDollar };

            // если поле Id определено (редактирование)
            if (vprId != null)
            {
                item.Name = name; // без New
                item.Id = (int)vprId.ConvertTo(typeof(int));
            }
            return item;
        }
    }
}