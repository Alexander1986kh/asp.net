﻿using ModelBindingAsp.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ModelBindingAsp.Controllers
{
    public class HomeController : Controller
    {



        ItemContext db = new ItemContext();
        public ActionResult Index()
        {
            return View(db.Items);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            Item book = db.Items.Find(id);
            if (book != null)
            {
                return View(book);
            }
            return HttpNotFound();
        }


        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Item book)
        {
            db.Items.Add(book);
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Create1([Bind(Include = "Name, Price")]Item book)
        {
            db.Items.Add(book);
            db.SaveChanges();

            return RedirectToAction("Index");
        }
        //перегенерировать представление

        //Можно использовать свойство Exclude   атрибута Bind,
        //чтобы исключить свойство из привязки
        [HttpPost]
        public ActionResult Create2([Bind(Exclude = "KDollar")]Item book)
        {
            db.Items.Add(book);
            db.SaveChanges();

            return RedirectToAction("Index");
        }


    }
}