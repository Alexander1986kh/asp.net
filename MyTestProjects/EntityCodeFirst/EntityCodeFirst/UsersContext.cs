﻿using EntityCodeFirst.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityCodeFirst
{
    public class UsersContext : DbContext
    {
        public UsersContext() : base("UsersContext")
        {
        }
        public DbSet<User> Users { set; get; }
        public DbSet<Address> Addresses { set; get; }
        public DbSet<Course> Courses { set; get; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<User>()
                .HasMany<Address>(u => u.Addresses)
                .WithRequired(a => a.User);

            modelBuilder.Entity<User>()
                .HasMany<Course>(u => u.Courses)
                .WithMany(c => c.Users);
                
        }

    }
}
