﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityCodeFirst.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Name { set; get; }
        public string LastName { get; set; }

        public virtual ICollection<Course> Courses { get; set; }
        public virtual ICollection<Address> Addresses { get; set; }
        public User()
        {
            Addresses = new List<Address>();
            Courses = new List<Course>();
        }
    }
}
