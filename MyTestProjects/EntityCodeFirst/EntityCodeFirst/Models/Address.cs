﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityCodeFirst.Models
{
    public class Address
    {
        public int Id { get; set; }
        public string Street { get; set; }
        public string Building { get; set; }
        public virtual User User { get; set; }
    }
}
