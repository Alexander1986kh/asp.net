namespace EntityTestProject
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ROLES
    {
        public int ID { get; set; }

        [Required]
        [StringLength(20)]
        public string ROLE_NAME { get; set; }

        [Required]
        [StringLength(20)]
        public string RULE { get; set; }
    }
}
