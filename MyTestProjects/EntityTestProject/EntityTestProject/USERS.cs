namespace EntityTestProject
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class USERS
    {
        public int ID { get; set; }

        [Required]
        [StringLength(20)]
        public string USER_NAME { get; set; }
    }
}
