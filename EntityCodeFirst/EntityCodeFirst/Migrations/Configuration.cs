namespace EntityCodeFirst.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using EntityCodeFirst.Models;
    using System.Collections.Generic;

    internal sealed class Configuration : DbMigrationsConfiguration<EntityCodeFirst.UsersContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(EntityCodeFirst.UsersContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
            if (context.Users.Count<User>() == 0)
            {
                context.Users.Add(new User()
                {
                    Name = "Semen",
                    LastName = "Seniavich",
                    Addresses = new List<Address>() {
                    new Address() { Street = "Sumskaya",Building= "21 A"},
                    new Address(){Street="Pushkinskaya", Building="19"}
                }
                });
                context.Users.Add(new User()
                {
                    Name = "Arhip",
                    LastName = "Gavriluk",
                    Addresses = new List<Address>() {
                    new Address() { Street = "Omskaya",Building= "1 A"},
                    new Address(){Street="Petrovskogo", Building="199"}
                }
                });
                context.SaveChanges();
            }
            if (context.Courses.Count() == 0)
            {
                context.Courses.Add(new Course()
                {
                    Name = "English"
                });
                context.Courses.Add(new Course()
                {
                    Name="Franch"
                });
                context.SaveChanges();
            }
        }
    }
}
