﻿using EntityCodeFirst.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EntityCodeFirst
{
    class Program
    {
        static void Main(string[] args)
        {
            using (UsersContext context = new UsersContext())
            {
                User user = context.Users.Find(1);
                Course course = context.Courses.
                    FirstOrDefault(c => c.Name == "Franch");
                if (user != null && course != null)
                {
                    user.Courses.Add(course);
                    context.SaveChanges();
                }
              
            }
            

        }
    }
}
