﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _010_TiresShop.Models.Persons
{
    public class Address
    {
        [Key]
        public int? Id { get; set; }
        [Required]
        public string Country { get; set; }
        [Required]
        public string City { get; set; }
        public string PostalCode { get; set; }
        [Required]
        public string Street { get; set; }
        [Required]
        public string Building { get; set; }
        public string Room { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
        public virtual Person Person { get; set; }
    }
}