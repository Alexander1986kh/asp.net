﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _010_TiresShop.Models.Persons
{
    public class Person
    {
        [Key]
        public int? Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Surname { get; set; }
        public string FatherName { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }

        public virtual ICollection<Address> Addresses { get; set; }
        public virtual ICollection<Contact> Contacts { get; set; }

        public Person()
        {
            Addresses = new List<Address>();
            Contacts = new List<Contact>();
        }
    }
}