﻿using _010_TiresShop.Models.Orders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _010_TiresShop.Models.Persons
{
    public class Client : Person 
    {
        public virtual ICollection<Order> Orders { get; set; }

        public Client()
        {
            Orders = new List<Order>();
        }
    }
}