﻿using System;
using System.ComponentModel.DataAnnotations;

namespace _010_TiresShop.Models.Products
{
    public class Product
    {
        [Key]
        public int? Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public decimal? SelfCostUsd { get; set; }
        [Required]
        public decimal? PriceUsd { get; set; }
        [Required]
        public decimal? Discount { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }


    }
}