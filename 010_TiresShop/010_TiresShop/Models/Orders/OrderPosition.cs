﻿using _010_TiresShop.Models.Products;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _010_TiresShop.Models.Orders
{
    public class OrderPosition
    {
        [Key]
        public int? Id { get; set; }
        [Required]
        public int? Count { get; set; }
        public virtual Product Product { get; set; }
        public virtual Order Order { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
    }
}