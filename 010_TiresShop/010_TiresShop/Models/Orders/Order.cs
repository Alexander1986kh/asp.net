﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Http;
using _010_TiresShop.Models.Orders.OrderEnums;
using _010_TiresShop.Models.Persons;
using _010_TiresShop.Models.Products;

namespace _010_TiresShop.Models.Orders
{
    public class Order
    {
        [Key]
        public int? Id { get; set; }
        [NotMapped]
        public decimal? TotalCost
        {
            get
            {
                return Positions.Select(p => p.Product.PriceUsd * p.Count).Sum();
            }
            private set { }
        }
        [Required]
        public PayMethod PayMethod { get; set; }
        public decimal? PartialPaySum { get; set; }
        public bool? PartialPaid { get; set; }
        public bool? TotalPaid { get; set; }
        [Required]
        public ApplicationUser Manager { get; set; }
        public virtual ICollection<OrderPosition> Positions { get; set; }
        public virtual Client Client { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }

        public Order()
        {
            Positions = new List<OrderPosition>(); 
        }
    }
}