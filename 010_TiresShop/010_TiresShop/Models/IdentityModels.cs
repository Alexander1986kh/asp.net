﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using _010_TiresShop.Models.Orders;
using _010_TiresShop.Models.Persons;
using _010_TiresShop.Models.Products;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;

namespace _010_TiresShop.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("TiresShopConnection", throwIfV1Schema: false)
        {   }

        public DbSet<Client> Clients { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<ContactType> ContactTypes { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<OrderPosition> OrderPositions { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Tire> Tires { get; set; }


        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Client>().Map(m =>
            {
                m.MapInheritedProperties();
                m.ToTable("Clients");
            });

            modelBuilder.Entity<Tire>().Map(m =>
            {
                m.MapInheritedProperties();
                m.ToTable("Tires");
            });

            modelBuilder.Entity<Contact>()
                .HasRequired(c => c.Type)
                .WithMany(t => t.Contacts);


            modelBuilder.Entity<Person>()
                .HasMany(p => p.Contacts)
                .WithOptional(c => c.Person);

            modelBuilder.Entity<Person>()
                .HasMany(p => p.Addresses)
                .WithOptional(a => a.Person);

            modelBuilder.Entity<Client>()
                .HasMany(c => c.Orders)
                .WithRequired(o => o.Client);

            modelBuilder.Entity<Order>()
                .HasMany(o => o.Positions)
                .WithRequired(op => op.Order);

            modelBuilder.Entity<Order>()
                .HasRequired(o => o.Manager);

            modelBuilder.Entity<OrderPosition>()
                .HasRequired(op => op.Product);
           
            



        }
    }
}