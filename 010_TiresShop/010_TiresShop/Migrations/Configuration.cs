namespace _010_TiresShop.Migrations
{
    using _010_TiresShop.Models;
    using _010_TiresShop.Models.Persons;
    using _010_TiresShop.Models.Products;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Models.Products.TireEnums;
    using _010_TiresShop.Models.Orders;
    using System.Diagnostics;
    using System.Data.Entity.Validation;
    using LoggersLibrary;

    internal sealed class Configuration : DbMigrationsConfiguration<_010_TiresShop.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(_010_TiresShop.Models.ApplicationDbContext context)
        {
            ContactType contactType1 = new ContactType() { Name = "Phone", Created = DateTime.Now, Updated = DateTime.Now };
            ContactType contactType2 = new ContactType() { Name = "Email", Created = DateTime.Now, Updated = DateTime.Now };
            ContactType contactType3 = new ContactType() { Name = "Skype", Created = DateTime.Now, Updated = DateTime.Now };
            ContactType contactType4 = new ContactType() { Name = "Viber", Created = DateTime.Now, Updated = DateTime.Now };
            ContactType contactType5 = new ContactType() { Name = "Telegram", Created = DateTime.Now, Updated = DateTime.Now };
            ContactType contactType6 = new ContactType() { Name = "Fax", Created = DateTime.Now, Updated = DateTime.Now };
            if (context.ContactTypes.Count() == 0)
            {
                context.ContactTypes.AddRange(new List<ContactType>()
                {
                    contactType1, contactType2, contactType3, contactType4, contactType5, contactType6
                });
                context.SaveChanges();
            }

            Client client1 = new Client()
            {
                Name = "Petr",
                Surname = "Knigin",
                FatherName = "Aliksandrovich",
                Addresses = new List<Address>()
                {
                    new Address() { Country = "Ukraine", PostalCode = "61000", City = "Kharkiv", Street = "Svetlaya", Building = "12", Room = "1", Created = DateTime.Now, Updated = DateTime.Now },
                    new Address() { Country = "Ukraine", PostalCode = "61000", City = "Kyiv", Street = "Klenovaya", Building = "123", Room = "5", Created = DateTime.Now, Updated = DateTime.Now }
                },
                Contacts = new List<Contact>() {
                    new Contact() { Type = contactType1, Value = "+380678962377", Created = DateTime.Now, Updated = DateTime.Now },
                    new Contact() { Type = contactType2, Value = "somemail@gmail.com", Created = DateTime.Now, Updated = DateTime.Now }
                },
                Created = DateTime.Now,
                Updated = DateTime.Now
            };
            Client client2 = new Client()
            {
                Name = "Konstantin",
                Surname = "Saprikin",
                Addresses = new List<Address>()
                {
                    new Address() { Country = "Ukraine", PostalCode = "61000", City = "Dnepr", Street = "Stroiteley", Building = "22", Room = "156", Created = DateTime.Now, Updated = DateTime.Now }
                },
                Contacts = new List<Contact>()
                {
                    new Contact() { Type = contactType1, Value = "+380685674565", Created = DateTime.Now, Updated = DateTime.Now },
                    new Contact() { Type = contactType2, Value = "home@ukr.net", Created = DateTime.Now, Updated = DateTime.Now },
                    new Contact() { Type = contactType2, Value = "mymail@gmail.com", Created = DateTime.Now, Updated = DateTime.Now }
                },
                Created = DateTime.Now,
                Updated = DateTime.Now
            };
            Client client3 = new Client()
            {
                Name = "Anna",
                Surname = "Komarova",
                FatherName = "Nikolaevna",
                Addresses = new List<Address>()
                {
                    new Address() { Country = "Ukraine", PostalCode = "61000", City = "Kharkiv", Street = "Kievskaya", Building = "324", Room = "55", Created = DateTime.Now, Updated = DateTime.Now }
                },
                Contacts = new List<Contact>()
                {
                    new Contact() { Type = contactType1, Value = "+380685674565", Created = DateTime.Now, Updated = DateTime.Now },
                    new Contact() { Type = contactType2, Value = "home@ukr.net", Created = DateTime.Now, Updated = DateTime.Now }
                },
                Created = DateTime.Now,
                Updated = DateTime.Now
            };
            Client client4 = new Client()
            {
                Name = "Anton",
                Surname = "Morozov",
                FatherName = "Dmitrievich",
                Addresses = new List<Address>()
                {
                     new Address() { Country = "Ukraine", PostalCode = "61000", City = "Vinnitsa", Street = "Zelenaya", Building = "45", Room = "11", Created = DateTime.Now, Updated = DateTime.Now },
                     new Address() { Country = "Ukraine", PostalCode = "61000", City = "Kyiv", Street = "Sportivnaya", Building = "77", Room = "2", Created = DateTime.Now, Updated = DateTime.Now }
                },
                Contacts = new List<Contact>()
                {
                    new Contact() { Type = contactType4, Value = "+380637785645", Created = DateTime.Now, Updated = DateTime.Now },
                    new Contact() { Type = contactType2, Value = "work@gmail.com", Created = DateTime.Now, Updated = DateTime.Now }
                },
                Created = DateTime.Now,
                Updated = DateTime.Now
            };
            Client client5 = new Client()
            {
                Name = "Anastasiya",
                Surname = "Klimchenko",
                FatherName = "Anatolievna",
                Addresses = new List<Address>()
                {
                    new Address() { Country = "Ukraine", PostalCode = "61000", City = "Kharkiv", Street = "Sumskaya", Building = "123", Room = "14", Created = DateTime.Now, Updated = DateTime.Now },
                    new Address() { Country = "Ukraine", PostalCode = "61000", City = "Lviv", Street = "Centralna", Building = "1", Room = "19", Created = DateTime.Now, Updated = DateTime.Now }
                },
                Contacts = new List<Contact>()
                {
                     new Contact() { Type = contactType5, Value = "+380681983477", Created = DateTime.Now, Updated = DateTime.Now },
                     new Contact() { Type = contactType6, Value = "+380443456787", Created = DateTime.Now, Updated = DateTime.Now },
                     new Contact() { Type = contactType3, Value = "my_skype", Created = DateTime.Now, Updated = DateTime.Now }
                },
                Created = DateTime.Now,
                Updated = DateTime.Now
            };

            if (context.Clients.Count() == 0)
            {
                context.Clients.AddRange(new List<Client>()
                {
                    client1, client2, client3, client4, client5
                });
                try
                {
                    context.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        LoggerLazyLoadSingleton.Instance.Use_D = true;

                        Debug.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Debug.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }

            }

            Tire tire1 = new Tire()
            {
                Name = "Fulda EcoControl 155/70 R13 75T",
                BrandName = "Fulda",
                AutomobileType = AutomobileType.Car,
                Season = TireSeason.Summer,
                Diameter = TireDiameter.R13,
                Profile = 70,
                Width = 155,
                CountryOrigin = "Poland",
                LoadIndex = 75,
                SpeedIndex = "T",
                YearManufactured = 2018,
                ModelName = "EcoControl",
                PriceUsd = 34.56M,
                SelfCostUsd = 29.38M,
                Discount = 0,
                Created = DateTime.Now,
                Updated = DateTime.Now
            };
            Tire tire2 = new Tire()
            {
                Name = "Barum Brillantis 2 155/70 R13 75T",
                BrandName = "Barum",
                AutomobileType = AutomobileType.Car,
                Season = TireSeason.Summer,
                Diameter = TireDiameter.R13,
                Profile = 70,
                Width = 155,
                CountryOrigin = "Czech Republic",
                LoadIndex = 75,
                SpeedIndex = "T",
                YearManufactured = 2018,
                ModelName = "Brillantis 2",
                PriceUsd = 30.45M,
                SelfCostUsd = 25.88M,
                Discount = 0,
                Created = DateTime.Now,
                Updated = DateTime.Now
            };
            Tire tire3 = new Tire()
            {
                Name = "Goodyear EfficientGrip Compact 195/65 R15 91T",
                BrandName = "Goodyear",
                AutomobileType = AutomobileType.Car,
                Season = TireSeason.Summer,
                Diameter = TireDiameter.R15,
                Profile = 65,
                Width = 195,
                CountryOrigin = "Poland",
                LoadIndex = 91,
                SpeedIndex = "T",
                YearManufactured = 2018,
                ModelName = "EfficientGrip Compact",
                PriceUsd = 49.5M,
                SelfCostUsd = 42.08M,
                Discount = 0,
                Created = DateTime.Now,
                Updated = DateTime.Now
            };
            if (context.Tires.Count() == 0)
            {
                context.Tires.AddRange(new List<Tire>()
                {
                    tire1, tire2, tire3
                });
                context.SaveChanges();
            }

            //OrderPosition op1 = new OrderPosition()
            //{
            //    Product = tire1,
            //    Count = 4
            //};
            //OrderPosition op2 = new OrderPosition()
            //{
            //    Product = tire2,
            //    Count = 8
            //};
            //OrderPosition op3 = new OrderPosition()
            //{
            //    Product = tire1,
            //    Count = 8
            //};
            //OrderPosition op4 = new OrderPosition()
            //{
            //    Product = tire3,
            //    Count = 2
            //};



        }
    }
}
