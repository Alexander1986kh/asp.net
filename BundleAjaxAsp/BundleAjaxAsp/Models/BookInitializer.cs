﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace BundleAjaxAsp.Models
{
    class BookInitializer : DropCreateDatabaseAlways<BookContext>
    {
        protected override void Seed(BookContext context)
        {
            context.Books.Add(new Book() { Name = "C++", Author = "Дейтел" });
            context.Books.Add(new Book() { Name = "C#", Author = "Шилдт" });
            base.Seed(context);
        }
    }
}