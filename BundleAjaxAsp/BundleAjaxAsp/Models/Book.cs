﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BundleAjaxAsp.Models
{
    public class Book
    {
        public int Id { get; set; }
        [Display(Name="Название книги")]
        public string Name { get; set; }
        public string Author { get; set; }
    }
}