﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AspNet1107.Models
{
    public class ApplicationUser : IdentityUser
    {
        public ApplicationUser()
        {
        }
    }
}