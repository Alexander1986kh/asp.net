﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace EntityTestProject
{
    class Program
    {
        static void Main(string[] args)
        {
            using (AdventureWorksContext context = new AdventureWorksContext())
            {
                //Загрузка всего содержимого таблицы 
                //context.Product.Load();
                //foreach(Product item in context.Product.Local)
                //Console.WriteLine(item.Name);

                //Поиск одного конкретного экземпляра по id
                //Product item=context.Product.Find(2);
                //Console.WriteLine(item.Name);

                //Поиск одного конкретного экземпляра по другому условию
                //Product item = context.Product.FirstOrDefault(x => x.Name == "Blade");
                //Console.WriteLine(item.Name);

                //Выборка
                //IQueryable<Product> items;
                //items = context.Product.Where<Product>(x => x.ProductID < 5);
                //foreach(var item in items)
                //{
                //    Console.WriteLine(item.Name);
                //}
                //Product product = new Product()
                //{
                //    Name = "Шина",
                //    ProductNumber = "1234567",
                //    MakeFlag = true,
                //    FinishedGoodsFlag = true,
                //    SafetyStockLevel = 1,
                //    ReorderPoint = 1,
                //    StandardCost = (decimal)1500.00,
                //    ListPrice = (decimal)1200.00,
                //    DaysToManufacture = 17,
                //    SellStartDate = DateTime.Now,
                //    rowguid = new Guid(),
                //    ModifiedDate = DateTime.Now
                //};
                //context.Product.Add(product);
                //context.SaveChanges();
                //Поиск одного конкретного экземпляра по другому условию
                Product item = context.Product.FirstOrDefault(x => x.Name == "Шинка");
                Console.WriteLine(item.Name);
                context.Product.Remove(item);
                context.SaveChanges();
            }

        }
    }
}
