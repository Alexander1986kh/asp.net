namespace EntityTestProject
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class MYLOG
    {
        [Key]
        public int MYLOGS_ID { get; set; }

        [Required]
        [StringLength(20)]
        public string ACTION { get; set; }

        [Required]
        [StringLength(20)]
        public string TBL_NAME { get; set; }

        public int ID_ROW { get; set; }

        [Required]
        [StringLength(100)]
        public string LOG_INFO { get; set; }

        [Column(TypeName = "date")]
        public DateTime LOG_TIME { get; set; }
    }
}
