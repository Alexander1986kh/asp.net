﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MyWebMVC3.Models
{
    public class PartContext : DbContext
    {
        public PartContext() : base("PartContext") { }
        public DbSet<Part> Parts { get; set; }
        // C - create
        // R - read
        // U - update
        // D - delete
        // E - edit
        public DbSet<Delivery> Deliveries { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Delivery>().HasMany(c => c.DeliveryParts)
                .WithMany(c => c.PartDeliveries)
                .Map(t => t.MapLeftKey("DeliveryId")
                .MapRightKey("PartId")
                .ToTable("DeliveryPart"));
        }
    }
}