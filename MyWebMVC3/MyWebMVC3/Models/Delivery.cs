﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyWebMVC3.Models
{
    public class Delivery
    {
        public int DeliveryId { get; set; }
        public string Address { get; set; }
        public virtual ICollection<Part> DeliveryParts { get; set; }
        public Delivery()
        {
            DeliveryParts = new List<Part>();
        }
    }
}