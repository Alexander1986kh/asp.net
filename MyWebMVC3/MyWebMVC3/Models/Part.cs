﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyWebMVC3.Models
{
    public class Part
    {
        public int PartId { get; set; }
        [Required(ErrorMessage ="Введите название продукта")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Введите цену продукта")]
        public double Price { get; set; }
        public string Descriprion { get; set; }
        public int Quantity { get; set; }
        public virtual ICollection<Delivery> PartDeliveries { get; set; }
        public Part()
        {
            PartDeliveries = new List<Delivery>();
        }
    }
}