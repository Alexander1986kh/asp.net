﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MyWebMVC3.Models
{
    public class PartDbInitializer : DropCreateDatabaseAlways<PartContext>
    {
        protected override void Seed(PartContext db)
        {
            db.Parts.Add(new Part() { PartId = 1, Name = "Карбюратор", Price = 15000.0f, Descriprion = "для BMW 3", Quantity = 1 });
            db.Parts.Add(new Part() { PartId = 1, Name = "Шины", Price = 4000.0f, Descriprion = "летние, для BMW 3", Quantity = 4 });
            db.Parts.Add(new Part() { PartId = 1, Name = "Шины", Price = 4000.0f, Descriprion = "зимние, для BMW 3", Quantity = 4 });
            db.Parts.Add(new Part() { PartId = 1, Name = "Шины", Price = 8000.0f, Descriprion = "шипованные, для BMW 3", Quantity = 4 });
            db.Parts.Add(new Part() { PartId = 1, Name = "Генератор", Price = 15000.0f, Descriprion = "для BMW 3", Quantity = 2 });
            base.Seed(db);
        }
    }
}