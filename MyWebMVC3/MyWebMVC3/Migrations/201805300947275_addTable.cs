namespace MyWebMVC3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Deliveries",
                c => new
                    {
                        DeliveryId = c.Int(nullable: false, identity: true),
                        Address = c.String(),
                    })
                .PrimaryKey(t => t.DeliveryId);
            
            CreateTable(
                "dbo.DeliveryPart",
                c => new
                    {
                        DeliveryId = c.Int(nullable: false),
                        PartId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.DeliveryId, t.PartId })
                .ForeignKey("dbo.Deliveries", t => t.DeliveryId, cascadeDelete: true)
                .ForeignKey("dbo.Parts", t => t.PartId, cascadeDelete: true)
                .Index(t => t.DeliveryId)
                .Index(t => t.PartId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DeliveryPart", "PartId", "dbo.Parts");
            DropForeignKey("dbo.DeliveryPart", "DeliveryId", "dbo.Deliveries");
            DropIndex("dbo.DeliveryPart", new[] { "PartId" });
            DropIndex("dbo.DeliveryPart", new[] { "DeliveryId" });
            DropTable("dbo.DeliveryPart");
            DropTable("dbo.Deliveries");
        }
    }
}
