namespace MyWebMVC3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Parts",
                c => new
                    {
                        PartId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Price = c.Double(nullable: false),
                        Descriprion = c.String(),
                    })
                .PrimaryKey(t => t.PartId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Parts");
        }
    }
}
