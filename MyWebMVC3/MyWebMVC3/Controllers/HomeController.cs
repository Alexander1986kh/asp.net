﻿using MyWebMVC3.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyWebMVC3.Controllers
{
    public class HomeController : Controller
    {
        PartContext db = new PartContext();

        [HttpGet]
        public ActionResult Index()
        {
            db.Parts.Load();
            var lst = db.Parts.Local.ToList<Part>();
            
            return View(lst);
        }
    
        public ActionResult Create()
        {
            return View();
        }

        public ActionResult Edit()
        {
            return View();
        }

        public ActionResult Delete()
        {
            return View();
        }
        public FileResult GetStream()
        {
            string path = Server.MapPath("~/Files/CE_3d_Int_WB.pdf");
            // Объект Stream
            FileStream fs = new FileStream(path, FileMode.Open);
            string file_type = "application/pdf";
            string file_name = "CE_3d_Int_WB.pdf";
            return File(fs, file_type, file_name);
        }
        public FileResult GetBytes()
        {
            string path = Server.MapPath("~/Files/CE_3d_Int_WB.pdf");
            byte[] mas = System.IO.File.ReadAllBytes(path);
            string file_type = "application/pdf";
            string file_name = "CE_3d_Int_WB.pdf";
            return File(mas, file_type, file_name);
        }
        public FileResult GetFile()
        {
            // Путь к файлу
            string file_path = Server.MapPath("~/Files/CE_3d_Int_WB.pdf");
            // Тип файла - content-type
            string file_type = "application/pdf";
            // Имя файла - необязательно
            string file_name = "CE_3d_Int_WB.pdf";
            return File(file_path, file_type, file_name);
        }
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            db.Dispose();
        }
    }
}