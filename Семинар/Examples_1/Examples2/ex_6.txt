using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Reflection;

namespace LINQ_1
{


    class Program
    {
        static void Main(string[] args)
        {
            foreach (string item in GetStringSubsetAsArray())
            {
                Console.WriteLine(item);
            } 

            Console.WriteLine();
            ReflectOverQueryResults(GetStringSubsetAsArray());

        }
        static void ReflectOverQueryResults(object resultSet)
        {
            Console.WriteLine("***** info about your query *****");
            Console.WriteLine("resultSet is of type: {0}", resultSet.GetType().Name); // ��� 
            Console.WriteLine("resultSet location: {0}",
            resultSet.GetType().Assembly.GetName().Name); // ������������ 
        }
        static string[] GetStringSubsetAsArray()
        {
            string[] colors = {"Light Red", "Green", "Yellow", "Dark Red", "Red", "Purple"};
            var theRedColors = from c in colors
                               where c.Contains("Red")
                               select c;
            // ���������� ���������� � ������, 
            return theRedColors.ToArray();
        } 

    }
}