          string[] currentVideoGames = { "Morrowind", "Uncharted 2", "Fallout 3", "Daxter", "System Shock 2" };
                  // ��������� ����������� �������� Funco � �������������� ��������� �������. 
            Func<string, bool> searchFilter =
            delegate(string game) { return game.Contains(" "); };
            Func<string, string> itemToProcess = delegate(string s) { return s; };
            // �������� �������� � ������ Enumerable, 
            var subset = currentVideoGames.Where(searchFilter)
            .OrderBy(itemToProcess).Select(itemToProcess);
            // ������� �� ������� ����������, 
            foreach (var game in subset)
                Console.WriteLine("Item: {0}", game);
            Console.WriteLine(); 