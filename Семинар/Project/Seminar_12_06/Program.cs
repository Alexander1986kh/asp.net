﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Collections;

namespace Seminar_12_06
{
    class Program
    {
        static void Main(string[] args)
        {
            // Examples.F_1();

            // Examples.F_2();//найти только первый четный, только последний четный

            //Поскольку все расширяет System.Object, все классы и структуры 
            //могут использовать это расширение. 
            // Examples.F_3();
            //  Examples.F_4();

            // В массиве строк найти  элементы массива,
            //включающих пробелы и вывести
            //в отсортированном порядке
            //Examples2.Ex1_1();
            //Console.WriteLine();
            // Examples2.Ex1_2();

            // Из массива целых чисел  сформировать массив?
            //где только элементы меньше 10. 
            //   Examples2.Ex2();

            // отложенного выполнение
            //   Examples2.Ex3();

            //  немедленноt выполнениt
            //Examples2.Ex4();

            // В классе (или структуре) можно определить поле, значением которого будет результат запроса LINQ.
            //LINQBasedField obj = new LINQBasedField();
            //obj.PrintGames();

            //IEnumerable<string> subset = Examples2.GetStringSubset();
            //foreach (string item in subset)
            //{
            //    Console.WriteLine(item);
            //}

            //Console.WriteLine();
            //Examples2.ReflectOverQueryResults(subset);

            //////////////////////ex_6
            //foreach (string item in Examples2.GetStringSubsetAsArray())
            //{
            //    Console.WriteLine(item);
            //}
            //Console.WriteLine();
            //Examples2.ReflectOverQueryResults(Examples2.GetStringSubsetAsArray());

            ///////////ex_7
            // Создать список Listo объектов Car. 
            //List<Car> myCars = new List<Car>() {
            //new Car{ PetName = "Henry", Color = "Silver", Speed = 100, Make = "BMW"},
            //new Car{ PetName = "Daisy", Color = "Tan", Speed = 90, Make = "BMW"},
            //new Car{ PetName = "Mary", Color = "Black", Speed = 55, Make = "VW" },
            //new Car{ PetName = "Clunker", Color = "Rust", Speed = 5, Make = "Yugo" },
            //new Car{ PetName = "Melvin", Color = "White", Speed = 43, Make = "Ford"}
            //};
            //GetFastCars(myCars);
            //Console.WriteLine();

            //  ex_8   Необобщенная коллекция автомобилей. 
            //ArrayList myCars = new ArrayList() {
            //new Car{ PetName = "Henry", Color = "Silver", Speed = 100, Make = "BMW"},
            //new Car{ PetName = "Daisy", Color = "Tan", Speed = 90, Make = "BMW"},
            //new Car{ PetName = "Mary", Color = "Black", Speed = 55, Make = "VW"},
            //new Car{ PetName = "Clunker", Color = "Rust", Speed = 5, Make = "Yugo" },
            //new Car{ PetName = "Melvin", Color = "White", Speed = 43, Make = "Ford"}
            //};
            // Трансформировать ArrayList в тип, совместимый с IEnumerable<T>. 
            //var myCarsEnum = myCars.OfType<Car>();
            //            // Создать выражение запроса, нацеленное на совместимый с IEnumerable<T> тип. 
            //            var fastCars = from c in myCarsEnum where c.Speed > 55 select c;
            //            foreach (var car in fastCars)
            //            {
            //                Console.WriteLine("{0} is going too fast!", car.PetName);
            //            }

            //            Console.WriteLine();

            List<string> myCars = new List<String> { "Yugo", "Aztec", "BMW" };
            List<string> yourCars = new List<String> { "BMW", "Saab", "Aztec" };
            // Получить общие члены. 
            var carlntersect = (myCars).Intersect(yourCars);

            Console.WriteLine("Here is what we have in common:");
            foreach (string s in carlntersect)
                Console.WriteLine(s); // Выводит Aztec и BMW.

        }

        static void GetFastCars(List<Car> myCars)
        {
            // Найти в контейнере Listo объекты Car, у которых Speed больше 55. 
            var fastCars = from c in myCars
                           where c.Speed > 55
                           select c;
            foreach (var car in fastCars)
            {
                Console.WriteLine("{0 } is going too fast!", car.PetName);
            }
        }
    }
        
   
    static class ObjectExtensions
    {
        // Определение расширяющего метода для System.Object. 
        public static void DisplayDefimngAssembly(this object obj)
        {
            Console.WriteLine("{0} lives here:\n->{1}\n",
                obj.GetType().Name,
            Assembly.GetAssembly(obj.GetType()));
        }
    }


    


}
