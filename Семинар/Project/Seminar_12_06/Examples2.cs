﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seminar_12_06
{
   public static class Examples2
    {
   

        public static void Ex1_1()
        {
            // Предположим, что имеется массив строк, 
            string[] currentVideoGames = { "Morrowind", "Uncharted 2", "Fallout 3", "Daxter", "System Shock 2" };
            string[] gamesWithSpaces = new string[5];
            for (int i = 0; i < currentVideoGames.Length; i++)
            {
                if (currentVideoGames[i].Contains(" "))
                    gamesWithSpaces[i] = currentVideoGames[i];
            }
            // Отсортировать набор. 
            Array.Sort(gamesWithSpaces);
            // Вывести на консоль результат, 
            foreach (string s in gamesWithSpaces)
            {
                if (s != null)
                    Console.WriteLine("Item: {0}", s);
            }
            Console.WriteLine();

        }
        public static void Ex1_2()
        {
            // Предположим, что имеется массив строк. 
            string[] currentVideoGames = { "Morrowind", "Uncharted 2", "Fallout 3", "Daxter", "System Shock 2" };
            // Выражение запроса для нахождения элементов массива, включающих пробелы. 
            IEnumerable<string> subset = from g in currentVideoGames
                                         where g.Contains(" ")
                                         orderby g
                                         select g;
            // Вывести на консоль результаты, 
            foreach (string s in subset)
                Console.WriteLine("Item: {0}", s);
        }
        ///////////  ex_2.txt --------------------------
        public static void Ex2()
        {
            int[] numbers = { 10, 20, 30, 40, 1, 2, 3, 8 };
            // Вывести только элементы меньше 10. 
            IEnumerable<int> subset = from i in numbers
                                      where i < 10
                                      select i;

            foreach (int li in subset)
                Console.WriteLine("Item: {0}", li);
            ReflectOverQueryResults(subset);
        }
        public static void Ex3()
        {
            int[] numbers = { 10, 20, 30, 40, 1, 2, 3, 8 };
            // Получить числа меньше 10. 
            var subset = from i in numbers where i < 10 select i;
            // Оператор LINQ здесь выполняется! 
            foreach (var i in subset)
                Console.WriteLine("{0} < 10", i);
            Console.WriteLine();
            // Изменить некоторые данные в массиве. 
            numbers[0] = 4;
            // Оператор LINQ снова выполняется! 
            foreach (var j in subset)
                Console.WriteLine("{0} < 10", j);
            Console.WriteLine();
            ReflectOverQueryResults(subset);
        }
        public static void Ex4()
        {
            int[] numbers = { 10, 20, 30, 40, 1, 2, 3, 8 };
            // Получить данные НЕМЕДЛЕННО как int[]. 
            int[] subsetAsIntArray =
            (from l in numbers where l < 10 select l).ToArray<int>();
            //все ok
            //foreach (var j in subsetAsIntArray)
            //    Console.WriteLine("{0} < 10", j);
            //Console.WriteLine();
            //subsetAsIntArray[0] = 4;
            //Console.WriteLine("Изменить некоторые данные в массиве");
            //foreach (var j in subsetAsIntArray)
            //    Console.WriteLine("{0} < 10", j);
            //Console.WriteLine();
            // Получить данные НЕМЕДЛЕННО как List<int>. 
            List <int> subsetAsListOfInts =
            (from i in numbers where i < 10 select i).ToList<int>();
            Console.WriteLine();
            ReflectOverQueryResults(subsetAsListOfInts);
        }
        public static IEnumerable<string> GetStringSubset()
        {
            string[] colors = { "Light Red", "Green", "Yellow", "Dark Red", "Red", "Purple" };
            // Обратите внимание, что subset представляет собой 
            // объект, совместимый с IEnumerable<string>. 
            IEnumerable<string> theRedColors = from c in colors
                                               where c.Contains("Red")
                                               select c;
            return theRedColors;
        }
        public static string[] GetStringSubsetAsArray()
        {
            string[] colors = { "Light Red", "Green", "Yellow", "Dark Red", "Red", "Purple" };
            var theRedColors = from c in colors
                               where c.Contains("Red")
                               select c;
            // Отобразить результаты в массив, 
            return theRedColors.ToArray();
        }


        public static void ReflectOverQueryResults(object resultSet)
        {
            Console.WriteLine("***** info about your query *****");
            Console.WriteLine("resultSet is of type: {0}", resultSet.GetType().Name); // тип 
            Console.WriteLine("resultSet location: {0}",
            resultSet.GetType().Assembly.GetName().Name); // расположение 
        }
    }
}
