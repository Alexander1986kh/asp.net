﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seminar_12_06
{
    class LINQBasedField
    {
        private static string[] currentVideoGames = {"Morrowind", "Uncharted 2",
"Fallout 3", "Daxter", "System Shock 2"};
        // Здесь нельзя использовать неявную типизацию, Нужно знать тип subset1 
        private IEnumerable<string> subset = from g in currentVideoGames
                                             where g.Contains(" ")
                                             orderby g
                                             select g;
        public void PrintGames()
        {
            foreach (var item in subset)
            {
                Console.WriteLine(item);
            }
        }
    }
}
