﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seminar_12_06
{
    static class Examples
    {
        public static void F_1()
        {
            // Неявно типизированные локальные переменные. 
            var mylnt = 0;
            var myBool = true;
            var myString = "Time, marches on...";
            // Вывод имен типов, лежащих в основе этих переменных. 
            Console.WriteLine("mylnt is a: {0}", mylnt.GetType().Name);
            Console.WriteLine("myBool is a: {0}", myBool.GetType().Name);
            Console.WriteLine("myString is a: {0}", myString.GetType().Name);
        }

        public static void F_2()
        {

            // Создать список целых. 
            List<int> list = new List<int>();
            list.AddRange(new int[] { 20, 1, 4, 8, 9, 44 });
            Console.WriteLine("Вывод на консоль всех чисел. :");
            foreach (int i in list)
            {
                Console.Write("{0}\t", i);
            }
            Console.WriteLine();
            // Лямбда-выражение С#. 
            List<int> evenNumbers = list.FindAll( i => (i % 2) == 0);
         
            Console.WriteLine("Вывод на консоль четных чисел. :");
            foreach (int evenNumber in evenNumbers)
            {
                Console.Write("{0}\t", evenNumber);
            }
            Console.WriteLine();
          
        }

        public static void F_3() { 
       // Поскольку все расширяет System.Object, все классы и структуры 
       // могут использовать это расширение. 
       int myInt = 12345678;
        myInt.DisplayDefimngAssembly();

        System.Data.DataSet d = new System.Data.DataSet();
        d.DisplayDefimngAssembly();
            Console.ReadLine();
        }
        public static void F_4()
        {
            var purchaseltem = new
            {
                TimeBought = DateTime.Now,
                ItemBought = new { Color = "Red", Make = "Saab", CurrentSpeed = 55 },
                Price = 34.000
            };
            Console.WriteLine(purchaseltem.TimeBought);
            Console.WriteLine(purchaseltem.Price);
        }
    }
}
